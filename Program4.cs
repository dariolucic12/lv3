﻿using System;

namespace zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification notification = new ConsoleNotification("Dario", "Pitanje", "kako si?",
                DateTime.Now, Category.INFO, ConsoleColor.Blue);

            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}