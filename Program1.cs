﻿using System;
using System.Collections.Generic;

namespace zadatak1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path1 = @"C:\Users\Korisnik\Desktop\aa.csv";
            Dataset data = new Dataset(path1);

            foreach(List<string> item in data.GetData()) {  
                foreach(string item2 in item) { 
                    Console.WriteLine(item2);
                }
            }
            Console.WriteLine("///////////CLONE//////////");

            Dataset data2 = (Dataset)data.Clone();

            foreach (List<string> item in data2.GetData())
            {
                foreach (string item2 in item)
                {
                    Console.WriteLine(item2);
                }
            }
            
        }
    }
}