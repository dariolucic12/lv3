﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak1
{
    interface Prototype
    {
        Prototype Clone();
    }
}
